package it.example.demo.service;

import it.example.demo.dao.SediDao;
import it.example.demo.model.Sedi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SediService {

    @Autowired
    SediDao sediDao;

    public List<Sedi> getSedi() {
        return sediDao.getSedi();
    }

    public Sedi getSedeById(Integer id) {
        return sediDao.getSediById(id);
    }

    public void saveOrUpdateSedi(List<Sedi> sedi) {
        sediDao.saveOrUpdateSedi(sedi);
    }

    public void deleteSedi(List<Sedi> sedi) {
        sediDao.deleteSedi(sedi);
    }

    public void deleteSedeById(Integer id) {
        sediDao.deleteSedeById(id);
    }
}
