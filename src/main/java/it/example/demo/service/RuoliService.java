package it.example.demo.service;

import it.example.demo.dao.RuoliDao;
import it.example.demo.model.Ruoli;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RuoliService {

    @Autowired
    RuoliDao ruoliDao;

    public List<Ruoli> getRuoli() {
        return ruoliDao.getRuoli();
    }

    public Ruoli getRuoloById(Integer id) {
        return ruoliDao.getRuoloById(id);
    }

    public void saveOrUpdateRuoli(List<Ruoli> ruoli) {
        ruoliDao.saveOrUpdateRuoli(ruoli);
    }

    public void deleteRuoli(List<Ruoli> ruoli) {
        ruoliDao.deleteRuoli(ruoli);
    }

    public void deleteRuoliById(Integer id) {
        ruoliDao.deleteRuoliById(id);
    }
}
