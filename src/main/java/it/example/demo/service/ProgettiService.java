package it.example.demo.service;

import it.example.demo.dao.ProgettiDao;
import it.example.demo.model.Progetti;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProgettiService {

    @Autowired
    private ProgettiDao progettiDao;

    public List<Progetti> getProgetti() {
        return progettiDao.getProgetti();
    }

    public Progetti getProjectById(Integer id) {
        return progettiDao.getProgettoById(id);
    }

    public void saveOrUpdateProgetti(List<Progetti> progettis) {
        progettiDao.saveOrUpdateProgetti(progettis);
    }

    public void deleteProgetti(List<Progetti> progetti) {
        progettiDao.deleteProgetti(progetti);
    }

    public void deleteProgettoById(Integer id) {
        progettiDao.deleteProgettoById(id);
    }
}
