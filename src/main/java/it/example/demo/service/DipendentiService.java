package it.example.demo.service;

import it.example.demo.dao.DipendentiDao;
import it.example.demo.model.Dipendenti;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DipendentiService {

    @Autowired
    private DipendentiDao dipendentiDao;

    public List<Dipendenti> getDipendenti(){
        return dipendentiDao.getDipendenti();
    }

    public Dipendenti getDipendenteById(Integer id){return dipendentiDao.getDipendenteById(id);}

    public void saveOrUpdateDipendenti(List<Dipendenti> dip) {
         dipendentiDao.saveOrUpdateDipendenti(dip);
    }

    public void deleteDipendenti(List<Dipendenti> dip) {
        dipendentiDao.deleteDipendenti(dip);
    }

    public void deleteDipendentiById(Integer id) {
        dipendentiDao.deleteDipendentiById(id);
    }


}
