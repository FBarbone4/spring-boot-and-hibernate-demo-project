package it.example.demo;

import it.example.demo.model.Dipendenti;
import it.example.demo.model.Progetti;
import it.example.demo.model.Ruoli;
import it.example.demo.model.Sedi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@SpringBootApplication
@EnableSwagger2
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public Docket swaggerConfig(){
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("it.example.demo"))
				.build()
				.apiInfo(metaInfo());
	}

	private ApiInfo metaInfo() {
		return  new ApiInfoBuilder()
				.version("1.0.0")
				.description("Progetto nato per affinare le abilità con spring boot e hibernate")
				.title("Spring Boot and Hibernate Demo Project")
				.build();
	}

}
