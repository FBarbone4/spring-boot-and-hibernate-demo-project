package it.example.demo.controller;

import com.sun.istack.NotNull;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.example.demo.model.Dipendenti;
import it.example.demo.model.Ruoli;
import it.example.demo.model.Sedi;
import it.example.demo.service.SediService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/office")
public class SediController {

    @Autowired
    SediService sediService;

    @GetMapping("/get-offices")
    @ApiOperation(value = "Permette di ritrovare tutti le sedi",
            notes = "Restituisce la lista completa delle sedi esistenti",
            response = Sedi.class,
            responseContainer = "list")
    public List<Sedi> getSedi(){
         return sediService.getSedi();
    }

    @GetMapping("/get-office/{id}")
    @ApiOperation(value = "Cerca una determinata sede dato l'id",
            notes = "Dato l'id ritrova la sede corrispondente",
            response = Sedi.class)
    public Sedi getSedeById(@ApiParam(value = "Id relativo alla sede da ritorvare",required = true)
                            @PathVariable("id") Integer id){return sediService.getSedeById(id);}

    @PostMapping("/save-offices")
    @ApiOperation(value = "Permette di salvare nuove sedi",
            notes = "Prende in input una lista di sedi")
    public void saveSedi(@ApiParam(value = "Lista sedi da salvare, può contenerne anche soltanto uno",required = true)
                         @RequestBody @NotNull List<Sedi> sedi){
        sediService.saveOrUpdateSedi(sedi);
    }

    @PutMapping("/update-offices")
    @ApiOperation(value = "Permette di aggiornare sedi",
            notes = "Prende in input una lista di sedi")
    public void updateSedi(@ApiParam(value = "Lista sedi da aggiornare, può contenerne anche soltanto uno",required = true)
                           @RequestBody @NotNull List<Sedi> sedi){
        sediService.saveOrUpdateSedi(sedi);
    }

    @DeleteMapping("/delete-offices")
    @ApiOperation(value = "Permette di eliminare sedi",
            notes = "Prende in input una lista di sedi")
    public void deleteSedi(@ApiParam(value = "Lista sedi da eliminare, può contenerne anche soltanto uno",required = true)
                           @RequestBody @NotNull List<Sedi> sedi){
        sediService.deleteSedi(sedi);
    }

    @DeleteMapping("/delete-office/{id}")
    @ApiOperation(value = "Permette di eliminare una sede",
            notes = "Dato l'id elimina la sede corrispondente")
    public void deleteSedeById(@ApiParam(value = "Id della sede da eliminare", required = true)
            @PathVariable("id") Integer id){
        sediService.deleteSedeById(id);
    }
}
