package it.example.demo.controller;

import com.sun.istack.NotNull;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.example.demo.model.Progetti;
import it.example.demo.model.Ruoli;
import it.example.demo.service.RuoliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rule")
public class RuoliController {

    @Autowired
    RuoliService ruoliService;

    @GetMapping("/get-rules")
    @ApiOperation(value = "Permette di ritrovare tutti i ruoli",
            notes = "Restituisce la lista completa dei ruoli esistenti",
            response = Ruoli.class,
            responseContainer = "list")
    public List<Ruoli> getRuoli(){
        return ruoliService.getRuoli();
    }

    @GetMapping("/get-rule/{id}")
    @ApiOperation(value = "Cerca un determinato ruolo",
            notes = "Dato l'id ritrova il ruolo corrispondente",
            response = Ruoli.class)
    public Ruoli getRuoloById(@ApiParam(value = "Id relativo al ruolo da ritorvare",required = true)
                              @PathVariable("id") Integer id){
        return ruoliService.getRuoloById(id);
    }

    @PostMapping("/save-rules")
    @ApiOperation(value = "Permette di salvare nuovi ruoli",
            notes = "Prende in input una lista di ruoli")
    public void saveRuoli(@ApiParam(value = "Lista ruoli da salvare, può contenerne anche soltanto uno",required = true)
                          @RequestBody @NotNull List<Ruoli> ruoli){
        ruoliService.saveOrUpdateRuoli(ruoli);
    }

    @PutMapping("/update-rules")
    @ApiOperation(value = "Permette di aggiornare ruoli",
            notes = "Prende in input una lista di ruoli")
    public void updateRuoli(@ApiParam(value = "Lista ruoli da aggiornare, può contenerne anche soltanto uno",required = true)
                            @RequestBody @NotNull List<Ruoli> ruoli){
        ruoliService.saveOrUpdateRuoli(ruoli);
    }

    @DeleteMapping("/delete-rules")
    @ApiOperation(value = "Permette di eliminare ruoli",
            notes = "Prende in input una lista di ruoli")
    public void deleteRuoli(@ApiParam(value = "Lista ruoli da eliminare, può contenerne anche soltanto uno",required = true)
                            @RequestBody @NotNull List<Ruoli> ruoli){
        ruoliService.deleteRuoli(ruoli);
    }

    @DeleteMapping("/delete-rule/{id}")
    @ApiOperation(value = "Permette di eliminare un ruolo",
            notes = "Dato l'id elimina il ruolo corrispondente")
    public void deleteRuoloById(@ApiParam(value = "Id del ruolo da eliminare", required = true)
                                @PathVariable("id") Integer id){
        ruoliService.deleteRuoliById(id);
    }
}
