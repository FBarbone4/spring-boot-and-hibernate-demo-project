package it.example.demo.controller;

import com.sun.istack.NotNull;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.example.demo.model.Progetti;
import it.example.demo.service.ProgettiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
public class ProgettiController {

    @Autowired
    private ProgettiService progettiService;

    @GetMapping("/get-projects")
    @ApiOperation(value = "Permette di ritrovare tutti i progetti",
            notes = "Restituisce la lista completa dei progetti esistenti",
            response = Progetti.class,
            responseContainer = "list")
    public List<Progetti> getProgetti(){
        return progettiService.getProgetti();
    }

    @GetMapping("/get-projet/{id}")
    @ApiOperation(value = "Cerca un determinato progetto",
            notes = "Dato l'id ritrova il progetto corrispondente",
            response = Progetti.class)
    public Progetti getProgettoById(@ApiParam(value = "Id relativo al progetto da ritorvare",required = true)
                                     @PathVariable("id") Integer id){
        return progettiService.getProjectById(id);
    }

    @PostMapping("/save-projects")
    @ApiOperation(value = "Permette di salvare nuovi progetti",
            notes = "Prende in input una lista di progetti")
    public void saveProgetti(@ApiParam(value = "Lista progetti da salvare, può contenerne anche soltanto uno",required = true)
                                 @RequestBody @NotNull List<Progetti> progetti){
        progettiService.saveOrUpdateProgetti(progetti);
    }

    @PutMapping("/update-projects")
    @ApiOperation(value = "Permette di aggiornare progetti",
            notes = "Prende in input una lista di progetti")
    public void updateProgetti(@ApiParam(value = "Lista progetti da aggiornare, può contenerne anche soltanto uno",required = true)
                               @RequestBody @NotNull List<Progetti> progetti){
        progettiService.saveOrUpdateProgetti(progetti);
    }

    @DeleteMapping("/delete-projects")
    @ApiOperation(value = "Permette di eliminare progetti",
            notes = "Prende in input una lista di progetti")
    public void deleteProgetti(@ApiParam(value = "Lista progetti da eliminare, può contenerne anche soltanto uno",required = true)
                               @RequestBody @NotNull List<Progetti> progetti){
        progettiService.deleteProgetti(progetti);
    }

    @DeleteMapping("/delete-project/{id}")
    @ApiOperation(value = "Permette di eliminare un progetto",
            notes = "Dato l'id elimina il progetto corrispondente")
    public void deleteProgettoById(@ApiParam(value = "Id relativo al progetto da eliminare",required = true)
                                  @PathVariable("id") Integer id){
        progettiService.deleteProgettoById(id);
    }
}
