package it.example.demo.controller;

import com.sun.istack.NotNull;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.example.demo.model.Dipendenti;
import it.example.demo.service.DipendentiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class DipendentiController {

    @Autowired
    private DipendentiService dipendentiService;

    @GetMapping("/get-employees")
    @ApiOperation(value = "Permette di ritrovare tutti i dipendenti",
            notes = "Restituisce la lista completa dei dipendenti esistenti",
            response = Dipendenti.class,
            responseContainer = "list")
    public List<Dipendenti> getDipendenti(){
        return dipendentiService.getDipendenti();
    }

    @GetMapping("/get-employee/{id}")
    @ApiOperation(value = "Cerca un determinato dipendente",
            notes = "Dato l'id ritrova il dipendente corrispondente",
            response = Dipendenti.class)
    public Dipendenti getDipendenteById(@ApiParam(value = "Id relativo al dipendente da ritrovare",required = true)
                                            @PathVariable("id") Integer id){
        return dipendentiService.getDipendenteById(id);
    }

    @PostMapping("/save-employees")
    @ApiOperation(value = "Permette di salvare nuovi dipendenti",
                notes = "Prende in input una lista di dipendenti")
    public void saveDipendenti(@ApiParam(value = "Lista dipendenti da salvare, può contenerne anche uno",required = true)
                                   @RequestBody @NotNull List<Dipendenti> dipendenti){
         dipendentiService.saveOrUpdateDipendenti(dipendenti);
    }

    @PutMapping("/update-employees")
    @ApiOperation(value = "Permette di aggiornare dipendenti",
                notes = "Prende in input una lista di dipendenti da aggiornare")
    public void updateDipendenti(@ApiParam(value = "Lista dipendenti da aggiornare, può contenerne anche soltanto uno",required = true)
                                     @RequestBody @NotNull List<Dipendenti> dipendenti){
        dipendentiService.saveOrUpdateDipendenti(dipendenti);
    }

    @DeleteMapping("/delete-employees")
    @ApiOperation(value = "Permette di eliminare dipendenti",
            notes = "Prende in input una lista di dipendenti da eliminare")
    public void deleteDipendenti(@ApiParam(value = "Lista di dipendenti da eliminare, può contenerne anche soltanto uno",required = true)
                                     @RequestBody @NotNull List<Dipendenti> dipendenti){
        dipendentiService.deleteDipendenti(dipendenti);
    }

    @DeleteMapping("/delete-employee/{id}")
    @ApiOperation(value = "Permette di eliminare un dipendente",
            notes = "Dato l'id elimina il dipendente corrispondente")
    public void deleteDipendenteById(@ApiParam(value = "Id dipendente da eliminare", required = true)
                                       @PathVariable("id") Integer id){
        dipendentiService.deleteDipendentiById(id);
    }
}
