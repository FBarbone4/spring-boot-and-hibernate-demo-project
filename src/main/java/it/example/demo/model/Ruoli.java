package it.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ruoli")
@ApiModel(description = "Dettagli ruolo")
public class Ruoli {

    @Id
    @Column(name = "id_ruolo")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "Identificatore del ruolo")
    private Integer id_ruolo;

    @ApiModelProperty(notes = "Effettiva nomenclatura del ruolo")
    @Column(name = "ruolo")
    private String ruolo;

    @OneToMany(mappedBy = "ruolo", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("ruolo")
    private List<Dipendenti> dipendenti;

    public Ruoli() {
    }

    public Integer getId_ruolo() {
        return id_ruolo;
    }

    public void setId_ruolo(Integer id_ruolo) {
        this.id_ruolo = id_ruolo;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public List<Dipendenti> getDipendenti() {
        return dipendenti;
    }

    public void setDipendenti(List<Dipendenti> dipendenti) {
        this.dipendenti = dipendenti;
    }

    @Override
    public String toString() {
        return "Ruoli{" +
                "id_ruolo=" + id_ruolo +
                ", ruolo='" + ruolo + '\'' +
                ", dipendenti=" + dipendenti +
                '}';
    }
}
