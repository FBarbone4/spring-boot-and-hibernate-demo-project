package it.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "dipendenti")
@ApiModel(description = "Anagrafica dipendente")
public class Dipendenti {

    @Id
    @Column(name = "id_dip")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "Identificatore del dipendente")
    private Integer idDip;

    @ApiModelProperty(notes = "Nome del dipendente")
    @Column(name = "nome")
    private String nome;

    @ApiModelProperty(notes = "Cognome del dipendente")
    @Column(name = "cognome")
    private String cognome;

    @ApiModelProperty(notes = "Data di nascita del dipendente")
    @Column(name = "nascita")
    @Temporal(TemporalType.DATE)
    private Date nascita;

    @ApiModelProperty(notes = "Residenza del dipendente")
    @Column(name = "residenza")
    private String residenza;

    @ApiModelProperty(notes = "Identificatore relativo alla sede del dipendente")
    @JoinColumn(name = "id_sede")
    @ManyToOne
    @JsonIgnoreProperties("dipendenti")
    private Sedi sede;

    @ApiModelProperty(notes = "Identificatore relativo al ruolo del dipendente")
    @JoinColumn(name = "id_ruolo")
    @ManyToOne
    @JsonIgnoreProperties("dipendenti")
    private Ruoli ruolo;

    @ApiModelProperty(notes = "Progetti sui quali lavora il dipendente")
    @ManyToMany(mappedBy = "dipendenti",
            fetch = FetchType.EAGER,
            targetEntity = Progetti.class)
    @JsonIgnoreProperties("dipendenti")
    private List<Progetti> progetti;

    public Dipendenti() {
    }

    public Integer getIdDip() {
        return idDip;
    }

    public void setIdDip(Integer idDip) {
        this.idDip = idDip;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getResidenza() {
        return residenza;
    }

    public void setResidenza(String residenza) {
        this.residenza = residenza;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Ruoli getRuolo() {
        return ruolo;
    }

    public void setRuolo(Ruoli ruolo) {
        this.ruolo = ruolo;
    }

    public Date getNascita() {
        return nascita;
    }

    public void setNascita(Date nascita) {
        this.nascita = nascita;
    }

    public Sedi getSede() {
        return sede;
    }

    public void setSede(Sedi sede) {
        this.sede = sede;
    }

    public List<Progetti> getProgetti() {
        return progetti;
    }

    public void setProgetti(List<Progetti> progettis) {
        this.progetti = progettis;
    }

    @Override
    public String toString() {
        return "Dipendenti{" +
                "idDip=" + idDip +
                ", nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                ", nascita=" + nascita +
                ", residenza='" + residenza + '\'' +
                ", sede=" + sede +
                ", ruolo=" + ruolo +
                ", progetti=" + progetti +
                '}';
    }

}
