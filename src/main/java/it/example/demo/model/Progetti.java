package it.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "progetti")
@ApiModel(description = "Dettagli progetto")
public class Progetti {

    @Id
    @Column(name = "id_progetto")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "Identificatore del progetto")
    private Integer idProgetto;

    @ApiModelProperty(notes = "Nome del progetto")
    @Column(name = "nome")
    private String nome;

    @ApiModelProperty(notes = "Data di partenza del progetto")
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @ApiModelProperty(notes = "Dominio del progetto")
    @Column(name = "argomento")
    private String argomento;

    @ApiModelProperty(notes = "Dipendenti impegnati sul progetto")
    @ManyToMany(fetch = FetchType.EAGER,
            targetEntity = Dipendenti.class)
    @JoinTable(name = "dipendenti_progetti",
            joinColumns = {@JoinColumn(name= "id_progetto")},
            inverseJoinColumns = {@JoinColumn(name= "id_dip")})
    @JsonIgnoreProperties("progetti")
    private List<Dipendenti> dipendenti;

    public Progetti() {
    }

    public Integer getIdProgetto() {
        return idProgetto;
    }

    public void setIdProgetto(Integer idProgetto) {
        this.idProgetto = idProgetto;
    }

    public List<Dipendenti> getDipendenti() {
        return dipendenti;
    }

    public void setDipendenti(List<Dipendenti> dipendenti) {
        this.dipendenti = dipendenti;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getArgomento() {
        return argomento;
    }

    public void setArgomento(String argomento) {
        this.argomento = argomento;
    }

    @Override
    public String toString() {
        return "Progetti{" +
                "idProgetto=" + idProgetto +
                ", nome='" + nome + '\'' +
                ", startDate=" + startDate +
                ", argomento='" + argomento + '\'' +
                ", dipendenti=" + dipendenti +
                '}';
    }
}
