package it.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sedi")
@ApiModel(description = "Dettagli sede")
public class Sedi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sede")
    @ApiModelProperty(notes = "Identificatore della sede")
    private Integer id_sede;

    @ApiModelProperty(notes = "Città di locazione della sede")
    @Column(name = "city")
    private String city;

    @ApiModelProperty(notes = "CAP della sede")
    @Column(name = "cap")
    private int cap;

    @ApiModelProperty(notes = "Via/Piazza della sede")
    @Column(name = "via")
    private String via;


    @OneToMany(mappedBy = "sede",fetch = FetchType.EAGER)
    @JsonIgnoreProperties("sede")
    private List<Dipendenti> dipendenti;

    public Sedi() {
    }

    public Integer getId_sede() {
        return id_sede;
    }

    public void setId_sede(Integer id_sede) {
        this.id_sede = id_sede;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public List<Dipendenti> getDipendenti() {
        return dipendenti;
    }

    public void setDipendenti(List<Dipendenti> dipendenti) {
        this.dipendenti = dipendenti;
    }

    @Override
    public String toString() {
        return "Sedi{" +
                "id_sede=" + id_sede +
                ", city='" + city + '\'' +
                ", cap=" + cap +
                ", via='" + via + '\'' +
                ", dipendenti=" + dipendenti +
                '}';
    }
}
