package it.example.demo.dao;

import it.example.demo.model.Ruoli;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class RuoliDao {

    @Autowired
    EntityManager entityManager;

    public List<Ruoli> getRuoli() {
        Session currSession = entityManager.unwrap(Session.class);
        return currSession.createQuery("FROM Ruoli", Ruoli.class).getResultList();
    }

    public Ruoli getRuoloById(Integer id) {
        Session currSession = entityManager.unwrap(Session.class);
        return currSession.find(Ruoli.class,id);
    }

    public void saveOrUpdateRuoli(List<Ruoli> ruoli) {
        Session currSession = entityManager.unwrap(Session.class);

        for(Ruoli r: ruoli){
            currSession.saveOrUpdate(r);
        }
    }

    public void deleteRuoli(List<Ruoli> ruoli) {
        Session currenSession = entityManager.unwrap(Session.class);
        for (Ruoli r: ruoli){
            currenSession.delete(currenSession.find(Ruoli.class,r.getId_ruolo()));
        }
    }

    public void deleteRuoliById(Integer id) {
        Session currentSession = entityManager.unwrap(Session.class);

        currentSession.delete(currentSession.find(Ruoli.class,id));
    }
}
