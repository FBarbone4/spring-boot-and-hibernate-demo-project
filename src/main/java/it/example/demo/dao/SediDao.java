package it.example.demo.dao;

import it.example.demo.model.Sedi;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class SediDao {

    @Autowired
    EntityManager entityManager;

    public List<Sedi> getSedi() {
        Session currentSession = entityManager.unwrap(Session.class);

        return currentSession.createQuery("FROM Sedi",Sedi.class).getResultList();
    }

    public Sedi getSediById(Integer id) {
        Session currentSession = entityManager.unwrap(Session.class);

        return  currentSession.find(Sedi.class,id);
    }

    public void saveOrUpdateSedi(List<Sedi> sedi) {
        Session currentSession = entityManager.unwrap(Session.class);

        for(Sedi s: sedi){
            currentSession.saveOrUpdate(s);
        }
    }

    public void deleteSedi(List<Sedi> sedi) {
        Session currentSession = entityManager.unwrap(Session.class);

        for(Sedi s: sedi){
            currentSession.delete(currentSession.find(Sedi.class,s.getId_sede()));
        }
    }

    public void deleteSedeById(Integer id) {
        Session currentSession = entityManager.unwrap(Session.class);

        currentSession.delete(currentSession.find(Sedi.class,id));
    }
}
