package it.example.demo.dao;

import it.example.demo.model.Dipendenti;
import it.example.demo.model.Progetti;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class ProgettiDao {
    @Autowired
    private EntityManager entityManager;

    public List<Progetti> getProgetti() {
        Session currentSession = entityManager.unwrap(Session.class);
        return currentSession.createQuery("FROM Progetti", Progetti.class).getResultList();
    }

    public Progetti getProgettoById(Integer id) {
        Session currentSession = entityManager.unwrap(Session.class);
        return currentSession.find(Progetti.class, id);
    }

    public void saveOrUpdateProgetti(List<Progetti> progetti) {
        Session currentSession = entityManager.unwrap(Session.class);
        for (Progetti p : progetti) {
            for (Dipendenti dipendenti : p.getDipendenti()) {
                currentSession.saveOrUpdate(dipendenti);
            }
            currentSession.saveOrUpdate(p);
        }
    }

    public void deleteProgetti(List<Progetti> progetti) {
        Session currentSession = entityManager.unwrap(Session.class);
        for (Progetti p : progetti) {
            currentSession.delete(currentSession.find(Progetti.class, p.getIdProgetto()));
        }

    }

    public void deleteProgettoById(Integer id) {
        Session currentSession = entityManager.unwrap(Session.class);
        currentSession.delete(currentSession.find(Progetti.class, id));
    }
}
