package it.example.demo.dao;

import it.example.demo.model.Dipendenti;
import it.example.demo.model.Progetti;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class DipendentiDao {

    @Autowired
    private EntityManager entityManager;

    public List<Dipendenti> getDipendenti(){
        Session currentSession = entityManager.unwrap(Session.class);
        return currentSession.createQuery("FROM Dipendenti", Dipendenti.class).getResultList();
    }

    public Dipendenti getDipendenteById(Integer id){
        Dipendenti d;
        Session currentSession = entityManager.unwrap(Session.class);
        d =currentSession.find(Dipendenti.class,id);
        return d;
    }

    public void saveOrUpdateDipendenti(List<Dipendenti> dipendenti) {
        Session currentSession = entityManager.unwrap(Session.class);

        Session updateSession = entityManager.unwrap(Session.class);

        for(Dipendenti e: dipendenti){

            for (Progetti p: e.getProgetti()){
                p.setDipendenti(dipendenti);
                updateSession.saveOrUpdate(p);
            }

            currentSession.saveOrUpdate(e);
        }

    }

    public void deleteDipendenti(List<Dipendenti> dipendenti) {
        Session currentSession = entityManager.unwrap(Session.class);
        for(Dipendenti e: dipendenti) {
            currentSession.delete(currentSession.find(Dipendenti.class,e.getIdDip()));
        }

    }

    public void deleteDipendentiById(Integer id) {
        Session currentSession = entityManager.unwrap(Session.class);
        currentSession.delete(currentSession.find(Dipendenti.class,id));
    }

}
